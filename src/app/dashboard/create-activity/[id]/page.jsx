import CreateOrder from "@/app/ui/form/CreateOrder";

export default function Page({ params }) {
    return <div><CreateOrder params = { params } /> </div>;
}