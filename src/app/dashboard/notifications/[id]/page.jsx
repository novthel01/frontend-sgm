import NotificationDetail from "@/app/ui/form/NotificationDetail";

export default function Page({ params }) {
    const id = params.id;

    return <div><NotificationDetail id = { id } /></div>;
}

