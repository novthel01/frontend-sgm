

export default function Page() {

    
    const toolList = [
        {
            code:'01',
            spareparts: 'rodamiento para eje motriz',
            quantity: '34',
            location: 'a-01'
        },
        {
            code:'02',
            spareparts: 'piston',
            quantity: '46',
            location: 'b-10'
        },
        {
            code:'03',
            spareparts: 'freno',
            quantity: '12',
            location: 'b-15'
        },
    ]

    return (
        <>
           <div className='rounded-md bg-blue-200 text-center my-12 text-blue-900 font-semibold text-xl p-1'>
                <h1>Store</h1>
            </div>

            <div className="container">
                <div className='table-responsive'>
                    <table className="table table-hover table-striped text-center w-90 m-auto " >
                        <thead className="rounded-md bg-blue-100 text-sm font-normal">
                            <tr>
                                <th scope="col" className="px-3 py-5 font-medium sm:pl-6">Code</th>
                                <th scope="col" className="px-3 py-5 font-medium">Spare Parts</th>
                                <th scope="col" className="px-3 py-5 font-medium">Quantity</th>
                                <th scope="col" className="px-3 py-5 font-medium">Location</th>
                            </tr>
                        </thead>

                        <tbody>
                            {toolList.map((t) => <tr key={t.code}>
                                                    <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{t.code}</td>
                                                    <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{t.spareparts}</td>
                                                    <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{t.quantity}</td>
                                                    <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{t.location}</td>
                                                </tr>
                            )}
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}