'use client'

import { getAllNotifications } from "@/api/ApiNotifications";
import { useState, useEffect } from "react";
import Link from "next/link";
import { CheckCircleIcon } from '@heroicons/react/24/outline';


export default function NotificationList() {

    const [notificationList, setNotificationList] = useState([]);


    useEffect(()=>{
            getAllNotifications()
            .then((res)=> {
                if(res.state === 'Ok'){
                    setNotificationList(res.data)
                }
                else{
                    alert(res.msj)
                }
            })
            .catch( error => console.log(error))
      
    },[])

    return (
        <>
            <div className='rounded-md bg-blue-200 text-center my-8 text-blue-900 font-semibold text-xl p-1'>
                <h1>NOTIFICATIONS</h1>
            </div>

            <div className="container">
                <div className='table-responsive'>
                    <table className="table table-hover table-striped text-center w-90 m-auto">
                        <thead className="rounded-md bg-blue-100 text-sm font-normal">
                            <tr>
                                <th scope="col" className="px-3 py-5 font-medium sm:pl-6">Date</th>
                                <th scope="col" className="px-3 py-5 font-medium">Description</th>
                                <th scope="col" className="px-3 py-5 font-medium">Machine</th>
                                <th scope="col" className="px-3 py-5 font-medium">Area</th>
                                <th scope="col" className="px-3 py-5 font-medium">Priority</th>
                                <th></th>
                                <th scope="col" className="px-3 py-5 font-medium">Status</th>
                            </tr>
                        </thead>

                        <tbody className="divide-y divide-gray-200 text-gray-900">
                            {notificationList.map((n) => <tr key={n._id}>
                                                
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{new Date (n.requirementdate).toLocaleDateString('en-US', {year: 'numeric', month: '2-digit', day: '2-digit'})}</td>
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{ n.descriptionproblem }</td>
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{ n.machine }</td>
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{ n.area }</td>
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm"><span className={ n.priority === 'priority 1'? "text-sm text-red-500": n.priority === 'priority 2'? "text-sm text-yellow-500" : "text-sm text-gray-800 " }>{ n.priority }</span></td>
                                                <td></td>
                                                <td className={ n.status === 'pending'? "bg-red-200 text-sm text-red-500": "bg-green-200 text-sm text-green-500" }>
                                                <Link href={`/dashboard/notifications/${n._id} `}>{ n.status }</Link></td>
                                            </tr>
                            )}   
                        </tbody>
                    </table>
                </div>
                   
            </div>
        </>
    );
}


