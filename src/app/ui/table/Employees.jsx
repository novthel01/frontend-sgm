'use client'

import Link from "next/link";
import { getAllUser } from "@/api/ApiUsers";
import { useState, useEffect } from 'react';


export default function Employees (){ 

    const [list, setList] = useState([]);
    const [query, setQuery] = useState("");

    useEffect(()=>{
        getAllUser()
            .then((res)=> {
                if(query.length === 0 || query.length < 2){
                    setList(res.data)
                }
                else{
                    const list = res.data
                    setList(list.filter((item) =>
                        keys.some((key) => item[key].toString().toLowerCase().includes(query))))
                }  
            }) .catch( error => console.log(error))
    },[query])

    return (
        <>
            <div className='rounded-md bg-blue-200 text-center my-8 text-blue-900 font-semibold text-xl p-1'>
                <h1>EMPLOYEES LIST</h1>
            </div>

            <div className="container">
                <div className="table-responsive">
                    <table className="table table-hover table-striped text-center w-90 m-auto">
                        <thead className="rounded-md bg-blue-100 text-sm font-normal">
                            <tr>
                                <th scope="col" className="px-3 py-5 font-medium sm:pl-6">Code Employee</th>
                                <th scope="col" className="px-3 py-5 font-medium">Employee name</th>
                                <th scope="col" className="px-3 py-5 font-medium">Position</th>
                            </tr>
                        </thead>

                        <tbody className="divide-y divide-gray-200 text-gray-900">
                            { list.map((e) => <tr key={ e._id }>
                                                    <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{ e.code }</td>
                                                    <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{ e.names } { e.lastnames }</td>
                                                    <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{ e.position }</td>
                                                    {/* <td><Link to={`/dashboard-manager/employees/${ e._id }`}></Link></td> */}
                                                </tr>
                            )}
                            
                        </tbody>
                    </table>
                </div>
                
            </div>
        </>
    );
}

