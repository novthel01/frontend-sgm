'use client'

import { getAllOrders } from "@/api/ApiOrders";
import { useState, useEffect } from 'react';
import Link from "next/link";



export default function ActivitiesList() {

    const [list, setList] = useState([]);
    const [query, setQuery] = useState("");



    useEffect(()=>{
        getAllOrders()
            .then((res)=> {
                if(query.length === 0 || query.length < 2){
                    setList(res.data)
                }
                else{
                    const list = res.data
                    setList(list.filter((item) =>
                        keys.some((key) => item[key].toString().toLowerCase().includes(query))))
                }  
            }) .catch( error => console.log(error))
    },[query])


    return (
        <>
            <div className='rounded-md bg-blue-200 text-center my-8 text-blue-900 font-semibold text-xl p-1'>
                <h1>MAINTENANCE ACTIVITIES</h1>
            </div>

            <div className="container">
                <div className="table-responsive">
                    <table className="table table-hover table-striped text-center w-90 m-auto">
                        <thead className="rounded-md bg-blue-100 text-sm font-normal">
                            <tr>
                                <th scope="col" className="px-3 py-5 font-medium sm:pl-6">Order N°</th>
                                <th scope="col" className="px-3 py-5 font-medium">Creation Date</th>
                                <th scope="col" className="px-3 py-5 font-medium">Maintenance Activity</th>
                                <th scope="col" className="px-3 py-5 font-medium">Status</th>
                            </tr>
                        </thead>

                        <tbody className="divide-y divide-gray-200 text-gray-900">
                            {list.map((o) => <tr key={o._id}>
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{o.ordernumber}</td>
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{new Date (o.ordercreationdate).toLocaleDateString()}</td>
                                                <td className="whitespace-nowrap bg-white px-4 py-5 text-sm">{o.activity}</td>
                                                <td className={ o.orderstatus === 'pending'? "bg-red-200 text-sm text-red-600": o.orderstatus === 'finalized'? "bg-green-200 text-sm text-green-600" : "bg-yellow-200 text-sm text-yellow-700 " }>
                                                <Link href={`/dashboard/maintenance-activities/${o._id} `}>{ o.orderstatus }</Link></td>
                                            </tr>
                            )}
                            
                        </tbody>
                    </table>
                </div>
                  
            </div>
        </>
    );
}


