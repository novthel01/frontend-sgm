'use client'

import { useEffect, useState } from 'react';
import { finishOrder, getOrderById } from '@/api/ApiOrders';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/navigation'


export default function OrderDetail({ params }) {

    const [ createby, setCreateby ] = useState('')
    const [ asignateto, setAsignateto ] = useState('')
    const [ code, setCode ] = useState('')
    const [ data, setData ] = useState({})
    const router = useRouter();

    const { register, handleSubmit, reset, formState: { errors } } = useForm({
        defaultValues: data
    });

    const finish =(data)=> {
        const orderstatus = 'finalized'
        const newData = {
            ...data, orderstatus
        }
        finishOrder(newData)
            .then((res)=> {
                alert(res.msj)
                router.push('/dashboard/maintenance-activities')
            })
            .catch(error=> console.log(error))
    }

    useEffect(() => {
        getOrderById(params.id)
            .then((res) => {
                const userInfo = res.data.userAssigneto
                const asignateto = `${ userInfo.names } ${ userInfo.lastnames } - [ ${ userInfo.position } ]`
                const usercreateby = res.data.createby
                const createby = `${ usercreateby.names } ${ usercreateby.lastnames }`
                setAsignateto(asignateto)
                setCreateby(createby)
                setCode(userInfo.code)
                setData(res.data.order)
                reset(res.data.order)
            
            })
            .catch(error => console.log(error))
    }, [params.id, reset]);


  return (
    <div className='w-full'>
        
        <form  className='w-11/12 rounded-xl border p-16 m-auto' onSubmit={ handleSubmit(finish) }>
            <legend className='text-center mb-8 text-blue-900 font-semibold text-xl md:text-2xl'>Maintenance Order</legend>
            
            <div className='flex justify-end -mx-3 mb-8'>
                <div className='w-1/5 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="ordernumber"><span>Order n°</span></label>
                    <input type="text" name='ordernumber' className='shadow appearance-none font-bold border rounded w-full py-2 px-3
                     text-gray-900 leading-tight text-right focus:outline-none focus:shadow-outline' {...register("ordernumber")} disabled />
                </div>
                
            </div>

            <div className='flex flex-wrap justify-between -mx-3 mb-3'>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="requestby"><span>Request by:</span></label>
                    <input type="text" name='requestby' className='appearance-none text-xs block w-full bg-gray-50
                        text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                        focus:bg-white'   {...register('requestby')} disabled />
                </div>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2"  htmlFor="area"><span>Area:</span></label>
                    <input type="text" name='area' className='appearance-none text-xs block w-full bg-gray-50
                    text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                    focus:bg-white' {...register('area')} disabled />
                </div>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="priority"><span>Priority</span></label>
                    <input type="text" name='priority' className='appearance-none text-xs block w-full bg-gray-50
                    text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                    focus:bg-white'  {...register("priority")} disabled />
                </div>
            </div>

            <div className='md:flex md:items-center mb-6'>
                <div className='w-full'>
                    <label className="block text-gray-700 text-sm font-semibold mb-2" htmlFor="descriptionproblem"><span>Description of the Problem: </span></label>
                    <textarea type="text" name='descriptionproblem' className='shadow bg-gray-50 appearance-none text-xs border
                     border-gray-200 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none 
                     focus:shadow-outlin'  rows={4} {...register('descriptionproblem')} disabled />
                </div>
            </div>

            <div className='flex flex-wrap -mx-3 mb-2'>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="areasupervisor"><span>Area Supervisor:</span></label>
                    <input type="text" name='areasupervisor' className='appearance-none text-xs block w-full
                     bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                      focus:outline-none focus:bg-white' {...register('areasupervisor')}  disabled />
                </div>
                <div className='w-full md:w-1/3 px-3'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="machine"><span>Machine:</span></label>
                    <input type="text" name='machine' className='appearance-none text-xs block w-full bg-gray-50
                     text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                      focus:bg-white' {...register('machine')} disabled />
                </div>
                <div className='w-full md:w-1/3 px-3'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="requirementdate"><span>Requirement Date:</span></label>
                    <input type="text" name='requirementdate' className='appearance-none text-xs block w-full bg-gray-50
                     text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                      focus:bg-white' value={ new Date (data.requirementdate).toLocaleDateString() } disabled />
                </div>
            </div>

            <div className='md:flex md:items-center mb-6'>
                <div className='w-full'>
                    <label className="block text-gray-700 text-sm font-semibold mb-2" htmlFor="spareparts"><span>Spare parts and special tools needed: </span></label>
                    <textarea type="text" name='spareparts' className='shadow bg-gray-50 text-xs appearance-none border border-gray-200 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outlin'  rows={4} {...register('spareparts',
                        {
                            maxLength: 2000
                        }) 
                        }/>
                        { errors.spareparts?.type === "maxLength" && <p className='text-danger small'>*Max length exceeded</p> }
                </div>
            </div>

            <div className='flex flex-wrap -mx-3 mb-4'>
                <div className='w-full px-3 mb-6 md:mb-0'>
                    <label className="block  tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="activity"><span>Maintenance Activity:</span></label>
                    <input type="text" name='activity' className='appearance-none text-xs block w-full bg-gray-50
                     text-gray-700 border border-gray-200 rounded py-2 px-4 mb-2 leading-tight focus:outline-none
                      focus:bg-white' {...register('activity')}  disabled />
                </div>
                
            </div>

            <div className='flex flex-wrap -mx-3 mb-2'>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="createby"><span>Create by:</span></label>
                    <input type="text" name='createby' className='appearance-none block w-full text-xs font-semibold bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white' defaultValue={ createby } disabled />
                </div>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="ordercreationdate"><span>Order Creation Date:</span></label>
                    <input type="text" name='ordercreationdate' className='appearance-none block w-full text-xs font-semibold bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white' 
                    value={ new Date (data.ordercreationdate).toLocaleDateString() }  disabled />
                </div>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="orderstatus"><span>Status:</span></label>
                    <input type="text" name='orderstatus' className='appearance-none block w-full text-xs font-semibold bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white' {...register('orderstatus')}  disabled />
                </div>
            </div>

            <div className='flex flex-wrap justify-between -mx-3 mb-2'>
                <div className='w-full md:w-1/2 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="asignateto"><span>Assigned to:</span></label>
                    <input type="text" name='asignateto' className='appearance-none block w-full text-xs font-semibold bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white' defaultValue={ asignateto } disabled />
                </div>
                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="code"><span>Code Employee:</span></label>
                    <input type="text" name='code' className='appearance-none block w-full text-xs font-semibold bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white' defaultValue={ code } disabled />
                </div>
            </div>

            <div className='flex flex-wrap mx-3 mb-2'>
                <div className='md:flex w-1/2 md:items-center p-2'>
                    <label className="block  tracking-wide text-gray-700 text-xs font-semibold w-1/3" htmlFor="startdate"><span>Start Date:</span></label>
                    <input type="text" name='startdate' className='appearance-none block w-1/2 text-xs font-semibold bg-gray-50 text-gray-700 border rounded p-1 mb-3 leading-tight focus:outline-none focus:bg-white'
                    value={ new Date (data.startdate).toLocaleDateString() } disabled />
                </div>
                {
                    data.enddate?
                    <div className='md:flex w-1/2 md:items-center p-2'>
                        <label className="block  tracking-wide text-gray-700 text-xs font-semibold w-1/3" htmlFor="enddate"><span>End Date:</span></label>
                        <input type="text" name='enddate' className='appearance-none block w-1/2 text-xs font-semibold bg-gray-50 text-gray-700 border rounded p-1 mb-3 leading-tight focus:outline-none focus:bg-white' 
                        defaultValue={ new Date (data.enddate).toLocaleDateString() } disabled />
                    </div>
                    :
                    <div className='md:flex w-1/2 md:items-center p-2'>
                        <label className="block  tracking-wide text-gray-700 text-xs font-semibold w-1/3" htmlFor="enddate"><span>End Date:</span></label>
                        <input type='date' name='enddate' className='appearance-none block w-1/2 text-xs font-semibold bg-gray-50 text-gray-700 border rounded p-1 mb-3 leading-tight focus:outline-none focus:bg-white'
                            {...register('enddate',
                                {
                                    required:true
                                }) 
                            }/>
                        { errors.enddate?.type === 'required' && <p className='text-danger small'>*Date is required</p> }
                    </div>
                }
            </div>

            <div className='md:flex md:items-center mb-6'>
                <div className='w-full'>
                    <label className="block text-gray-700 text-sm font-semibold mb-2" htmlFor="comments"><span>Comments: </span></label>
                    <textarea type="text" name='comments' className='shadow bg-gray-50 text-xs appearance-none border border-gray-200 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outlin' rows={4} {...register('comments',
                        {
                            maxLength: 2000
                        }) 
                        }/>
                        {errors.comments?.type === "maxLength" && <p className='text-danger small'>*Max length exceeded</p> }
                </div>
            </div>
           
            <div>
                {
                    data.orderstatus !=='finalized'?
                        <button type='submit' className='shadow bg-blue-900 items-center hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded'>Finalize</button>
                        :
                    null
                }
            </div>
               
        </form>           
    </div>
   
  )
}
