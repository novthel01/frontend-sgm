'use client'

import { useForm } from 'react-hook-form';
import { useState, useEffect } from 'react';
import { getNotificationsById, processNotification, newNotification } from '@/api/ApiNotifications';
import { er } from '../utils/RegularExpression';
import { useRouter } from "next/navigation";



export default function NotificationDetail( params ) {

    const id= params.id
    const [requestData, setRequestData] = useState({});
    const router = useRouter();

    const { register, handleSubmit, reset, formState: { errors } } = useForm({
        defaultValues : requestData
    });

   

    const onSubmit = data => {
        if(params.id){
            const status = 'processed'
            const newData = { ...data, status }
            console.log(newData)
            processNotification(newData)
                .then((res)=>{
                    router.push(`/dashboard/create-activity/${ data._id }`)
                })
                .catch(error => console.log(error))
        }else {
            const notificationnumber = 0
            const newData = { 
                ...data, 
                notificationnumber
             }
            newNotification(newData)
                .then((res)=>{
                    alert(res.msj)
                    reset()
                })
                .catch(error => console.log(error))
        }
    }

    
    useEffect(()=>{
            getNotificationsById(id)
                .then((res)=>{ 
                    setRequestData(res.data)
                    reset(res.data)
                })
                .catch(error => console.log(error))
        
    },[id, reset])
   

    return ( 
        <div className='w-full'>

            <form  className='w-8/12 rounded-xl border p-16 m-auto' onSubmit={ handleSubmit( onSubmit )} >
                <legend className='text-center mb-12 text-blue-900 font-semibold text-xl md:text-2xl '>MAINTENANCE REQUEST</legend>
               
                <div className='md:flex justify-between my-2'>
                    <div className='md:w-1/2 p-1'>
                            <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="requestby"><span>Request by:</span></label>
                            <input type="text" name='requestby' className='appearance-none text-xs block w-full
                            bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                                focus:outline-none focus:bg-white' disabled={ id? 'disabled': ''}  {...register('requestby',
                        {
                            required:true,
                            pattern: er.text,
                            maxLength:150
                        }) 
                        }/>
                    </div>

                    <div className='md:w-1/2 p-1'>
                        <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="area"><span>Area:</span></label>
                        <input type="text" name='area' className='appearance-none text-xs block w-full
                     bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                      focus:outline-none focus:bg-white' disabled={ id? 'disabled': ''} {...register('area',
                        {
                            required:true,
                            maxLength: 100
                        }) 
                        }/>
                        { errors.area?.type === 'required' && <p className='text-danger small'>*Area is required</p> }
                        {errors.area?.type === "maxLength" && <p className='text-danger small'>*Max length exceeded</p> }
                    </div>
                </div>

                <div className='md:flex md:items-center my-2'>
                    <div className='md:w-1/2 p-1'>
                        <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="areasupervisor"><span>Area Supervisor:</span></label>
                        <input type="text" name='areasupervisor' className='appearance-none text-xs block w-full
                     bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                      focus:outline-none focus:bg-white' disabled={ id? 'disabled': ''} {...register('areasupervisor',
                        {
                            required:true,
                            pattern: er.text,
                            maxLength: 150
                        }) 
                        }/>
                        { errors.areasupervisor?.type === 'required' && <p className='text-danger small'>*Area Supervisor is required</p> }
                        { errors.areasupervisor?.type === 'pattern' && 
                        <p className='text-danger small'>
                            * only lowercase, uppercase, accents, and spaces.
                        </p> }
                        {errors.areasupervisor?.type === "maxLength" && <p className='text-danger small'>*Max length exceeded</p> }
                    </div>
                </div>

                <div className='md:flex justify-between my-2'>
                    <div className='md:w-1/2'>
                        <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="machine"><span>Machine:</span></label>
                        <input type="text" name='machine' className='appearance-none text-xs block w-full
                     bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                      focus:outline-none focus:bg-white' disabled={ id? 'disabled': ''} {...register('machine',
                        {
                            required:true,
                            maxLength: 250
                        }) 
                        }/>
                        { errors.machine?.type === 'required' && <p className='text-danger small'>*Machine is required</p> }
                        {errors.machine?.type === "maxLength" && <p className='text-danger small'>*Max length exceeded</p> }
                    </div>  

                    {
                        id?
                        (
                            <div className='md:w-1/3'>
                                <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="requirementdate"><span>Requirement date:</span></label>
                                <input type="text" name='requirementdate' className='appearance-none text-xs block w-full
                                    bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                                    focus:outline-none focus:bg-white' 
                                 value={ new Date (requestData.requirementdate).toLocaleDateString('en-US', {year: 'numeric', month: '2-digit', day: '2-digit'}) }
                                  disabled  />
                            </div>
                        )
                        :
                        null
                    }   
                </div>

                <div className='md:w-1/3 my-4'>
                    {
                        id?
                        (
                            <div>
                                <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="priority"><span>Priority:</span></label>
                                <input type="text" name='priority' className='appearance-none text-xs block w-full
                                bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                                focus:outline-none focus:bg-white' disabled {...register('priority')} />
                            </div>
                        )
                        :
                        (
                            <div>
                                <select className="appearance-none text-xs block w-full
                                    bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                                    focus:outline-none focus:bg-white" id="priority-order" {...register("priority")}>
                                    <option  className='option-selected' value="Priority 1">Priority 1</option>
                                    <option  className='option-value' value="Priority 2">Priority 2</option>
                                    <option  className='option-value' value="Priority 3">Priority 3</option>
                                </select>
                            </div>   
                        )
                    }
                </div>

                <div className='md:flex md:items-center mb-6'>
                    <div className='w-full'>
                        <label className="block text-gray-700 text-sm font-semibold mb-2" htmlFor="descriptionproblem"><span>Description of the problem: </span></label>
                        <textarea type="text" name='descriptionproblem' className='shadow bg-gray-50 text-xs appearance-none border border-gray-200 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outlin' rows={4} disabled={ id? 'disabled': ''} 
                        {...register('descriptionproblem',
                            {
                                required:true,
                                maxLength: 2000
                            }) 
                        }/>
                        { errors.descriptionproblem?.type === 'required' && <p className='text-danger small'>*Description of the Problem is required</p> }
                        {errors.descriptionproblem?.type === "maxLength" && <p className='text-danger small'>*Max length exceeded</p> }
                    </div>
                </div>

                {
                    id? 
                    (
                        <div className='sec-btn-send col-3'>
                            {
                                requestData.status !=='processed'?
                                    <button className="shadow bg-blue-900 items-center hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">Process</button>
                                    :
                                    null
                            }
                         
                        </div> 
                    )
                    :
                    <div className='sec-btn-send col-3'>
                        <button className="shadow bg-blue-900 items-center hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type='submit' >SEND</button>
                    </div> 
                } 
            </form>           
        </div>
    )
}
