'use client'
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/navigation'
import { er } from '../utils/RegularExpression';
import Link from 'next/link';
import { Login } from '@/api/ApiUsers';
import {  useState } from 'react';



const FormLogin = () => {
    
    const { register, handleSubmit, formState: { errors } } = useForm({});
    const [error, setError] = useState(false);
    const [msjError, setmsjError] = useState();
    const router = useRouter();

        

    const onSubmit = (data) => {
        Login(data)
            .then(res => {
                if(res.state === 'Ok'){
                    localStorage.setItem('token', res.accessToken );
                    const url= res.url
                    router.push(url)
                
                }else{
                    setError(true);
                    setmsjError(res.msj)
                    setTimeout(() => setError(false), 3000);
                }
            })
            .catch(error => console.log(error))   
    }


    return (
        <div className="w-full max-w-xs">
            <form className='bg-white shadow-2xl rounded-2xl px-8 pt-6 pb-8 mb-4' id='formLogin' onSubmit={ handleSubmit( onSubmit ) } >
                
                <legend className='text-center mt-4 bg-clip-text text-transparent bg-gradient-to-r from-blue-400 to-violet-800 font-bold text-2xl'>WELCOME</legend>
                <div className='my-6'>
                    <label className='block text-blue-900 text-sm font-semibold mb-2' htmlFor="username"><span>User *</span></label>
                    <input type="text" name='username' className='text-xs font-semibold shadow appearance-none border rounded w-full py-2 px-3
                     text-gray-700 leading-tight focus:outline-none focus:shadow-outline' {...register("username", 
                    { 
                        required:true,
                        pattern: er.user
                    })
                    } />
                    { errors.username?.type === 'required' && <p className='text-xs text-red-600 font-medium mt-3'>*username is required</p> }
                    { errors.username?.type === 'pattern' && 
                    <p className='text-xs text-red-600 font-medium mt-3'>
                        *Lowercase letters, numbers, underscore, and hyphen. Usernames must be between 3 and 24 characters
                    </p> }
                </div>
                
                <div className='mb-6'>
                    <label className='block text-blue-900 text-sm font-semibold mb-2' htmlFor="password"><span>Password *</span></label>
                    <input type="password" name='password' className='shadow appearance-none  border rounded w-full py-2 px-3
                     text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline'  {...register('password',{ required:true }) }/>
                    { errors.password?.type === 'required' && <p className='text-xs text-red-600 font-medium mt-3'>*Password is required</p> }
                </div>

                <div className='flex items-center justify-between'>
                    <button className="bg-blue-500 hover:bg-blue-700 text-white m-auto font-semibold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                        Sign In
                    </button>
                </div>

                <div className='mt-4'>
                    <Link className='inline-block align-baseline text-xs text-semibold text-blue-900 hover:text-blue-800' href=''>Forgot Password?</Link>
                </div> 
                { error && <div className='text-sm text-center text-red-600 font-medium mt-3' role= 'alert'>{ msjError }</div> }
            </form>
        </div>
       
    )
}

export default FormLogin;