'use client'

import { useForm } from "react-hook-form";
import { useEffect, useState } from 'react';
import { getUserByRole } from "@/api/ApiUsers";
import { newOrder } from "@/api/ApiOrders";
import { useRouter } from "next/navigation";
import { er } from "../utils/RegularExpression";
import { getNotificationsById } from "@/api/ApiNotifications";


export default function CreateOrder({ params }) {


    const [ListTechnician, setListTechnician] = useState([]);
    const [ListLeader, setListLeader] = useState([]);
    const [data, setData] = useState([]);
    const router = useRouter();

    const { register, handleSubmit, reset, formState: { errors } } = useForm({ defaultValues: data });

    const onSubmit = data => {
        
        const ordercreationdate = new Date()
        const ordernumber = 0

        const newData = {
            ...data, ordercreationdate,
            ordernumber
        }

        newOrder(newData)
            .then((res)=>{
                alert(res.msj) 
                reset()
                router.push('/dashboard/maintenance-activities')
            })
            .catch(error => console.log(error))
    }
    


    useEffect(()=>{
        if(params){
            getNotificationsById(params.id)
            .then(res =>{
               console.log(res)
                setData(res.data)
                reset(res.data)
            })
        }
        getUserByRole('technician')
            .then(res => setListTechnician(res.data))
            .catch(error => console.log(error))
        getUserByRole('leader')
            .then(res => setListLeader(res.data))
            .catch(error => console.log(error))
    },[params, reset])
    

  return (
   

    <div className='w-full'>
        <form  className='w-11/12 rounded-xl border p-16 m-auto' onSubmit={ handleSubmit( onSubmit )} >
            <legend className='text-center mb-8 text-blue-900 font-semibold text-xl md:text-2xl '>Create Maintenance Work Order</legend>

            <div className='md:flex md:items-center my-2'>
                <div className='md:w-1/3' hidden>
                    <label htmlFor="ordernumber"><span>Order n°</span></label>
                    <input type="text" name='ordernumber' className='shadow appearance-none border rounded w-full py-2 px-3
                     text-gray-700 leading-tight focus:outline-none focus:shadow-outline' {...register("ordernumber")}/>
                </div>

                <div className="inline-block relative w-40 rounded-xl">
                    <select className="block w-full bg-white border border-gray-400 text-slate-900 hover:border-gray-500 px-4 py-2 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline"
                      {...register("priority")}>
                        <option  className='option-selected' value="Priority 1">Priority 1</option>
                        <option  className='option-value' value="Priority 2">Priority 2</option>
                        <option  className='option-value' value="Priority 3">Priority 3</option>
                    </select>
                </div>   
            </div>

            <br></br>

            <div className='flex flex-wrap -mx-3 mb-2'>

                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="requestby"><span>Request by:</span></label>
                    <input type="text" name='requestby' className='appearance-none text-xs block w-full bg-gray-50
                     text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                      focus:bg-white' disabled={ params? 'disabled': '' } {...register('requestby',
                    {
                        required:true,
                        pattern: er.text,
                        maxLength:150
                    }) 
                    }/>
                    { errors.requestby?.type === 'required' && <p className='text-red-500 text-sm'>*Request by is required</p> }
                    { errors.requestby?.type === 'pattern' && 
                    <p className='text-red-500 text-sm'>
                        * only lowercase, , accents, and spaces.
                    </p> }
                    {errors.requestby?.type === "maxLength" && <p className='text-red-500 text-sm'>*Max length exceeded</p> }
                </div>

                <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                    <label  className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="area"><span>Area:</span></label>
                    <input type="text" name='area' className='appearance-none text-xs block w-full bg-gray-50
                    text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                    focus:bg-white' disabled={ params? 'disabled': '' } {...register('area',
                    {
                        required:true,
                        maxLength:100
                    }) 
                    }/>
                    { errors.area?.type === 'required' && <p className='text-red-500 text-sm'>*Area is required</p> }
                    {errors.area?.type === "maxLength" && <p className='text-red-500 text-sm'>*Max length exceeded</p> }
                </div>


                {
                    params?
                    <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                        <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="requirementdate"><span>Requirement Date:</span></label>
                        <input type="text" name='requirementdate' className='appearance-none text-xs block w-full bg-gray-50
                                text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                                focus:bg-white' 
                        defaultValue={ new Date ().toLocaleDateString() } disabled />
                    </div>
                    :
                    <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
                        <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="requirementdate"><span>Requirement Date:</span></label>
                        <input type='date' name='requirementdate' className='appearance-none text-xs block w-full bg-gray-50
                                text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                                focus:bg-white'
                                {...register('requirementdate',
                                    {
                                        required:true
                                    }) 
                                }/>
                        { errors.requirementdate?.type === 'required' && <p className='text-danger small'>*Date is required</p> }
                    </div>
                }
            </div>

            <div className='md:flex md:items-center mb-2'>
                <div className='w-full'>
                    <label className="block text-gray-700 text-sm font-semibold mb-2" htmlFor="descriptionproblem"><span>Description of the Problem: </span></label>
                    <textarea type="text" name='descriptionproblem' className='shadow bg-gray-50 appearance-none text-xs border
                     border-gray-200 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none 
                     focus:shadow-outlin' rows={4} {...register('descriptionproblem',
                    {
                        required:true,
                        maxLength: 2000
                    }) 
                    }/>
                    { errors.descriptionproblem?.type === 'required' && <p className='text-red-500 text-sm'>*Description of the Problem is required</p> }
                    {errors.descriptionproblem?.type === "maxLength" && <p className='text-red-500 text-sm'>*Max length exceeded</p> }
                </div>
            </div>

            <div className='flex flex-wrap -mx-3 mb-2'>
                <div className='w-full md:w-1/2 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="areasupervisor">Area Supervisor:</label>
                    <input type="text" name='areasupervisor' className='appearance-none text-xs block w-full
                     bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight
                      focus:outline-none focus:bg-white'  {...register('areasupervisor',
                    {
                        required:true,
                        pattern: er.text,
                        maxLength: 150
                    }) 
                    }/>
                    { errors.areasupervisor?.type === 'required' && <p className='text-red-500 text-sm'>*Area Supervisor is required</p> }
                    { errors.areasupervisor?.type === 'pattern' && 
                    <p className='text-red-500 text-sm'>
                        * only lowercase, , accents, and spaces.
                    </p> }
                    {errors.areasupervisor?.type === "maxLength" && <p className='text-red-500 text-sm'>*Max length exceeded</p> }
                </div>

                <div className='w-full md:w-1/2 px-3'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="machine"><span>Machine:</span></label>
                    <input type="text" name='machine' className='appearance-none text-xs block w-full bg-gray-50
                     text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none
                      focus:bg-white' {...register('machine',
                    {
                        required:true,
                        maxLength: 250
                    }) 
                    }/>
                    { errors.machine?.type === 'required' && <p className='text-red-500 text-sm'>*Machine is required</p> }
                    {errors.machine?.type === "maxLength" && <p className='text-red-500 text-sm'>*Max length exceeded</p> }
                </div>
                
            </div>

            <div className='flex flex-wrap -mx-3 mb-2'>
                <div className='w-full px-3 mb-6 md:mb-0'>
                    <label className="block  tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="activity"><span>Maintenance Activity:</span></label>
                    <input type="text" name='activity' className='appearance-none text-xs block w-full bg-gray-50
                     text-gray-700 border border-gray-200 rounded py-2 px-4 mb-2 leading-tight focus:outline-none
                      focus:bg-white' {...register('activity',
                    {
                        required:true,
                        maxLength: 300
                    }) 
                    }/>
                    { errors.activity?.type === 'required' && <p className='text-red-500 text-sm'>*Maintenance Activity is required</p> }
                    {errors.activity?.type === "maxLength" && <p className='text-red-500 text-sm'>*Max length exceeded</p> }
                </div>  
            </div>

            <div className='flex flex-wrap -mx-3 mb-2'>
                <div className='w-full md:w-1/2 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="createby"><span>Create by:</span></label>
                    <select className='appearance-none block w-full text-xs font-semibold bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white' {...register("createby",
                    {
                        required:true,
                    })
                    } >
                        <option value="">-- Select Leader --</option>
                        {
                            ListLeader.map(u => <option className='option-tech' key={u._id} value={u._id} > { u.names } { u.lastnames }</option>)
                        }
                    </select>
                    { errors.asignateto?.type === 'required' && <p className='text-red-500 text-sm'>*Asigned to is required</p> }
                </div>
                
                <div className='w-full md:w-1/2 px-3 mb-6 md:mb-0'>
                    <label className="block tracking-wide text-gray-700 text-sm font-semibold mb-2" htmlFor="asignateto"><span>Assigned to:</span></label>
                    <select className='appearance-none block w-full text-xs font-semibold bg-gray-50 text-gray-700 border border-gray-200 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white'  {...register("asignateto",
                    {
                        required:true,
                    })
                    } >
                        <option value="">-- Select Technician --</option>
                        {
                            ListTechnician.map(u => <option className='option-tech' key={u._id} value={u._id} > { u.names } { u.lastnames } -  { u.position } </option>)
                        }
                    </select>
                    { errors.asignateto?.type === 'required' && <p className='text-red-500 text-sm'>*Asigned to is required</p> }
                </div>

            </div>

            <div className='flex flex-wrap mb-3'>
                <div className='md:flex w-1/2 md:items-center p-2'>
                    <label className="block  tracking-wide text-gray-700 text-xs font-semibold w-1/3" htmlFor="startdate"><span>Start Date:</span></label>
                    <input type="date" name='startdate' className='appearance-none block w-1/2 text-xs font-semibold bg-gray-50 text-gray-700 border rounded p-1 mb-3 leading-tight focus:outline-none focus:bg-white' {...register('startdate',
                    {
                        required:true
                    }) 
                    }/>
                    { errors.startdate?.type === 'required' && <p className='text-red-500 text-sm'>*Date is required</p> }
                </div>

                <div className='md:flex w-1/2 md:items-center p-2'>
                    <label className="block  tracking-wide text-gray-700 text-xs font-semibold w-1/3" htmlFor="enddate"><span>End Date:</span></label>
                    <input type="date" name='enddate' className='appearance-none block w-1/2 text-xs font-semibold bg-gray-50 text-gray-700 border rounded p-1 mb-3 leading-tight focus:outline-none focus:bg-white' {...register('enddate')} />
                </div>
            </div>

            <div className='md:flex md:items-center mb-6'>
                <div className='w-full'>
                    <label className="block text-gray-700 text-sm font-semibold mb-2" htmlFor="spareparts"><span>Spare parts and special tools needed: </span></label>
                    <textarea type="text" name='spareparts' className='shadow bg-gray-50 text-xs appearance-none border border-gray-200 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outlin' rows={4} {...register('spareparts',
                    {
                        maxLength: 2000
                    }) 
                    }/>
                    {errors.spareparts?.type === "maxLength" && <p className='text-red-500 text-sm'>*Max length exceeded</p> }
                </div>
            </div>

            <div>
                <button className="shadow bg-blue-900 items-center hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                    CREATE
                </button>
            </div>
                 
        </form>               
    </div>
  )
}