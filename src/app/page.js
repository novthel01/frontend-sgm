import FormLogin from "./ui/form/FormLogin";


export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24 bg-gradient-to-r from-sky-100 to-indigo-300">
      <div>
        <FormLogin />
      </div>
    </main>
  );
}
